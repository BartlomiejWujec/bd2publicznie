CREATE TABLE IF NOT EXISTS band (
   id serial PRIMARY KEY,
   name VARCHAR UNIQUE NOT NULL,
   creationDate DATE not NULL
);
INSERT INTO band
VALUES
   (DEFAULT, 'Pink Floyd', '01/09/1964'),
   (DEFAULT, 'Genesis', '01/06/1967'),
   (DEFAULT, 'Queen', '02/06/1971');