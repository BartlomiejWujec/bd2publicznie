// Do zdefiniowania aplikacji użyjemy Express.js
const express = require('express');
const app = express();

// Nasza aplikacja będzie „konsumowała” dane w formacie JSON
app.use(express.json());

//==================================================================
// Definiujemy REST API – kod poniżej wymaga uzupełnienia
//==================================================================

// Pobieranie danych na temat wszystkich zespołów
app.get('/bands', async (req, res) => {
  const queryResponse = await client.query("SELECT * FROM band;"); 

  return res.send({
    allGroups: [...queryResponse.rows]
  });
});

// Dodawanie rekordów do bazy
app.post('/bands', async (req, res) => {
  console.log(req.body);
  if (req.body.name && req.body.creationDate){
  const queryResponse = await client.query(`INSERT INTO band VALUES(DEFAULT, '${req.body.name}', '${req.body.creationDate}');`)
  .then(x => res.status(200), y => res.status(400));
  } else res.status(400);
  const message = {
    toInsert: req.body
  };
  return res.send(message);
});

// Pobieranie danych na temat zespołu o danej nazwie
app.get('/bands/:bandName', async (req, res) => {
  let name = req.params.bandName;
  const queryResponse = await client.query(`SELECT * FROM band WHERE name='${name}';`);
  const band = queryResponse.rows[0] ?? {} 
  return res.send({
    data: band
  });
});

// Usuwanie rekordu związanego z zespołem
app.delete('/bands/:id', async (req, res) => {
  let id = req.params.id;
  if (id) {
  const queryResponse = await client.query(`DELETE FROM band WHERE id=${id};`)
  } else res.status(400);
  return res.send({
    deletedBandId: id
  });
});

// Aktualizacja rekordu związanego z zespołem
app.put('/bands/:id', async (req, res) => {
  let id = req.params.id;
  let data = req.body;
  const keys = Object.keys(data); const values = Object.values(data);
  let statement = '';
  for (let i = 0; i<keys.length; i++){
    if (i != keys.length-1) statement+=`${keys[i]}='${values[i]}', `;
    else statement+=`${keys[i]}='${values[i]}'`;
  }
  console.log(statement);
  const queryResponse = await client.query(`UPDATE band SET ${statement} WHERE id=${id};`);
  return res.send({
    updatedBandId: id,
    data
  });
});

//==================================================================
// Poniższy kod nie powinien już wymagać zmian
//==================================================================

// Przygotowujemy/wczytujemy konfigurację połączenia z PostgreSQL-em
require('dotenv').config();
const dbConnData = {
    host: process.env.PGHOST || '127.0.0.1',
    port: process.env.PGPORT || 5432,
    database: process.env.PGDATABASE || 'postgres',
    user: process.env.PGUSER || 'postgres',
    password: process.env.PGPASSWORD
};
// Łączymy się z bazą i „stawiamy” serwer API
// Do kontaktu z serwerem PostgreSQL wykorzystamy bibliotekę pg

const { Client } = require('pg');
const client = new Client(dbConnData);
console.log('Connection parameters: ');
console.log(dbConnData);
client
  .connect()
  .then(() => {
    console.log('Connected to PostgreSQL');
    const port = process.env.PORT || 5000
    app.listen(port, () => {
      console.log(`API server listening at http://localhost:${port}`);
    });
  })
  .catch(err => console.error('Connection error', err.stack));
