require('dotenv').config();
const dbConnData = {
    host: process.env.PGHOST || '127.0.0.1',
    port: process.env.PGPORT || 5432,
    database: process.env.PGDATABASE || 'postgres',
    user: process.env.PGUSER || 'postgres',
    password: process.env.PGPASSWORD
};

const { Client } = require('pg');
const client = new Client(dbConnData);
console.log('Connection parameters: ');
console.log(dbConnData);
client
.connect()
.then(() => {
    console.log('Connected to PostgreSQL');
    const port = process.env.PORT || 5000
})
.catch(err => console.error('Connection error', err.stack));



const fs=require("fs");
client.query(`SELECT * FROM band;`).then((result)=>{
    const currentYear=new Date().getYear();
    fs.writeFile("output.csv", `id;name;creationDate;years\n`+result.rows.map(({id, name, creationdate: creationDate})=>{
        return `${id};${name};${creationDate};${creationDate};${currentYear-creationDate.getYear()}`;
    }).join("\n"), ()=>{});
});