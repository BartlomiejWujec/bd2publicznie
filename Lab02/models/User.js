const { Schema, model } = require('mongoose');

// Schema domyślnie dodaje unikalne pole _id, dlatego pomijamy je w deklaracji
const userSchema = new Schema({
    login: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        match: /.*@.*\..*/,
        unique: true
    },
    registrationDate: Date,
});

module.exports = model('User', userSchema);