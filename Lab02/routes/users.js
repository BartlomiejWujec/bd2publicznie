const { response } = require('express');
const express = require('express');
const router = express.Router();

const User = require('../models/User');

router.get('/', async (req, res) => {
    const users = await User.find({});
    return res.send({
        allUsers: users
      });
});

router.post('/', async (req, res) => {
    const {login, email, registrationDate} = req.body;
    const toInsert = new User({login, email, registrationDate});
    toInsert.save().then(res => {}).catch(err => console.log(err));
    return res.send(req.body);
});

router.get('/:id', async (req, res) => {
    const user = await User.findById(req.params.id);
    return res.send(user);
});

router.put('/:id', async (req, res) => {
    const id = req.params.id;
    const response = await User.findByIdAndUpdate(id, req.body);
    return res.send({ 
      putUserId: id
    });
});

router.delete('/:id', async (req, res) => {
  const id = req.params.id;
  const response = await User.findByIdAndRemove(id);
  return res.send({
    deletedUserId: id
  });
});

router.patch('/:id', async (req, res) => {
  const id = req.params.id;
  const response = await User.findByIdAndUpdate(id, req.body);
    return res.send({
      patchUserId: id
    });
});

module.exports = router;
