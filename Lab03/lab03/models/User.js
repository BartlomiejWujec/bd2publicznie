const { Schema, model } = require('mongoose');

const userSchema = new Schema({
    login: String,
    email: String,
    registrationDate: Date,
    posts: [{type: String, ref: 'Post'}]
});

module.exports = model('User', userSchema);