const express = require('express');
const router = express.Router({mergeParams: true});

const Post = require('../models/Post');
const User = require('../models/User');

router.get('/', async (req, res) => {
  const posts = await Post.find({});
  return res.send({
    posts: [...posts]
  });
});

router.get('/summary', async (req, res) => {
  const result = await Post.aggregate([
    {
      "$group": {
        "_id": null,
        "sum": { "$sum": "$responses" },
        "avg": { "$avg": "$responses" },
        "min": { "$min": "$responses" },
        "max": { "$max": "$responses" }
      }
    },
    {
      "$project": {
        "_id": 0
      }
    }
  ]);

  res.send({
    summary: result[0]
  })
})

router.get('/authors', async (req, res) => {
  const result = await User.aggregate([
    {
      "$unwind": "$posts"
    },
    {
    "$group": {
      "_id": "$email",
      "count": { "$sum": 1}
    }
  }])
  res.send({
    result
  })
})

router.get('/:id', async (req, res) => {
  const post = await User.findById(req.params.id);
  return res.send(post);
});

router.post('/:userId', async (req, res) => {
    const {text, responses} = req.body;
    const toInsert = new Post({text, responses, author: req.params.userId});
    toInsert.save().then(
      x => {
        User.findByIdAndUpdate(req.params.userId, {'$addToSet': {posts: x._id}})
       .then(x => {}, y => {if (y) console.log(y)})},
      y => {if (y) console.log(y)});
    return res.send(req.body);
});

router.delete('/:id', async (req, res) => {
  const id = req.params.id;
  await Post.findByIdAndDelete(req.params.id);
  return res.send({
    deletedUserId: id
  });
});


module.exports = router;
