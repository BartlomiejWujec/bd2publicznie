const express = require('express');
const router = express.Router();

const User = require('../models/User');

router.get('/', async (req, res) => {
  const users = await User.find({});
  const populated = await User.populate(users, "posts");
  return res.send({
      allUsers: populated
    });
});

router.post('/', async (req, res) => {
  const {login, email, registrationDate} = req.body;
  const toInsert = new User({login, email, registrationDate});
  toInsert.save().then(res => {}).catch(err => console.log(err));
  return res.send(req.body);
});

router.get('/registration-raport', async (req, res) => {  
  const date = req.query.date
  const jsDate = new Date(date)

  const result = await User.aggregate([{
    "$match": { "registrationDate": { "$gt": jsDate }}
  },
  {
    "$group": { "_id": "$registrationDate", count: { "$sum": 1}}
  }]);

  res.send({
    registrationRaport: result
  });
}) ;

router.get('/:id', async (req, res) => {
  const user = await User.findById(req.params.id);
  return res.send(user);
});

router.put('/:id', async (req, res) => {
  const id = req.params.id;
  const response = await User.findByIdAndUpdate(id, req.body);
  return res.send({ 
    putUserId: id
  });
});

router.delete('/:id', async (req, res) => {
  const id = req.params.id;
  const response = await User.findByIdAndRemove(id);
  return res.send({
    deletedUserId: id
  });
});

router.patch('/:id', async (req, res) => {
const id = req.params.id;
const response = await User.findByIdAndUpdate(id, req.body);
  return res.send({
    patchUserId: id
  });
});


module.exports = router;
