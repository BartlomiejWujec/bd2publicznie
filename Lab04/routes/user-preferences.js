const express = require('express');
const router = express.Router({mergeParams: true});
const client = require('../config/redisClient');

router.get('/', async (req, res) => {
  const response = await client.keys('user-preferences:*')
  .catch(promiseErr => {if (promiseErr) console.log(promiseERR)});

  return res.send({response});
});

router.get('/:key', async (req, res) => {
  const response = await client.get(`user-preferences:${req.params.key}`)
  .catch(promiseErr => {if (promiseErr) console.log(promiseERR)});

  return res.send({
    key: `user-preferences:${req.params.key}`,
    value: response
  });
});

router.post('/', async (req, res) => {
  const {key, value, expiration} = req.body;

  if (!expiration) {
    const response = await client.set(`user-preferences:${key}`, value)
    .catch(x => console.log(x));
    return res.send(response);
  } else {
    const response = await client.set(`user-preferences:${key}`, value, "EX", expiration)
    .catch(x => console.log(x));
    return res.send(response);
  }
});

router.put('/:key', async (req, res) => {
  const {key, value, expiration} = req.body;

  if (!expiration) {
    const response = await client.set(`user-preferences:${key}`, value)
    .catch(x => console.log(x));
    return res.send(response);
  } else {
    const response = await client.set(`user-preferences:${key}`, value, "EX", expiration)
    .catch(x => console.log(x));
    return res.send(response);
  }
});

router.delete('/:key', async (req, res) => {
  const key = req.params.key;
  
  const response = await client.del(`user-preferences:${key}`);
  
  return res.send({
    response
  });
});


module.exports = router;
