const express = require('express');
const router = express.Router();
const client = require('../config/redisClient');

router.get('/', async (req, res) => {
  const response = await client.lrange(`user-queue`, 0, -1)
  return res.send({
      allUsers: [...response]
    });
});

router.post('/', async (req, res) => {
  const { value } = req.body;
  try {
  const response = await client.rpush(`user-queue`, value).catch(x => console.log(x));
  } catch (e) {
    console.log(e);
    res.statusCode(400).send("FAIL")
  }
  return res.send("OK");
});

router.get('/:range', async (req, res) => {
  const range = req.params.range;
  const response = await client.lrange('user-queue', 0, range-1);

  return res.send({
    response
  });
});


router.delete('/', async (req, res) => {
  const response = await client.lpop('user-queue');
  return res.send({
    poppedUser: response
  });
});

module.exports = router;
