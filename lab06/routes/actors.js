const { response } = require('express');
const express = require('express');
const router = express.Router({mergeParams: true});
const driver = require('../config/neo4jDriver');

router.get('/', async (req, res) => {
  const session = driver.session();
  await session
  .run(`MATCH (actor: Actor) RETURN actor`)
  .then( result => {
    res.statusCode = 200;
    res.send({
      "actors": result.records.map(x => {
        return {"id": x._fields[0].identity.low,
        "properties": x._fields[0].properties}
      })
    })
  })
  .catch( err => {
    console.log(err);
    res.statusCode = 400;
    return res.send({
      "actors": []
    })
  })
  .then(()=>{
    session.close()
  })
});

router.get('/:id', async (req, res) => {
  const session = driver.session();
  await session
  .run(`MATCH (actor: Actor) WHERE ID(actor) = ${req.params.id} RETURN actor`)
  .then(result => {
    res.statusCode = 200;
    res.send({
      "actors": result.records.map(x => {
        return {"id": x._fields[0].identity.low,
        "properties": x._fields[0].properties}
      })
    });
  })
  .catch(err => {
    console.log(err);
    res.statusCode = 400;
    res.send({})
  })
});

router.post('/', async (req, res) => {
  const {name, age, company} = req.body
  const session = driver.session();
  await session
      .run(`MERGE (actor: Actor {name: '${name}', age: ${age}, company: '${company}'}) RETURN actor`)
      .then(result => {
        res.send({
          "actor": result.records[0]._fields[0]
        })
        session.close();
      })
});

router.put('/:id', async (req, res) => {
  const { name, age, company } = req.body;
  const session = driver.session();
  await session.run(`MATCH (actor: Actor) WHERE ID(actor) = ${req.params.id} SET actor.name = '${name}', actor.age = ${age}, actor.company = '${company}' RETURN *`)
  .then(response => {
    if(response.records === []){
      res.statusCode=400;
      return res.send({});
    }
    else{
      return res.send({changedActor: response.records[0]._fields[0]});
    }
  })
  .catch(err => {
    console.log(err);
    res.statusCode=400;
    return res.send({});
  })
  .then(()=>{
    session.close();
  })
});

router.delete('/:id', async (req, res) => {
    const session = driver.session();
    await session
    .run(`MATCH (actor: Actor) WHERE ID(actor) = ${req.params.id} DELETE actor RETURN *`)
    .then(response => {
      if (response.records === []){
        res.statusCode=400;
        res.statusMessage="Actor doesn't exist";
        return res.send()
      }
      else{
        res.statusCode=200;
        return res.send({"deletedActor": response.records[0]._fields[0]})
      }
    })
    .catch(err => {
      console.log(err);
      res.statusCode=400;
      res.send()
    })
    .then(() => {
      session.close()
    })
    return res.send({});
});

module.exports = router;
