const express = require('express');
const router = express.Router({mergeParams: true});
const driver = require('../config/neo4jDriver');

router.get('/', async (req, res) => {
    const session = driver.session();
    try {
        const result = await session.readTransaction((tx) =>
            tx.run("MATCH (m:Movie) RETURN m"));
            
        session.close();

        const respond = result.records.map(record => {
            return record._fields[0];
        });
        return res.send(respond);

    } catch(ex) {
        res.send(ex);
    }
});

router.get('/:id', async (req, res) => {
    const session = driver.session();   
    try {
        const result = await session.readTransaction((tx) =>
            tx.run(`MATCH (m:Movie) WHERE ID(m) = ${req.params.id} RETURN m`));
            
        session.close();

        const respond = result.records.map(record => {
            return record._fields[0];
        });
        return res.send(respond);

    } catch(ex) {
        res.send(ex);
    }
});


router.post('/', async (req, res) => {
    const { title, releaseDate, genre } = req.body;
    const session = driver.session();
    try {
        const response = await session.writeTransaction((tx) => 
            tx.run(`MERGE (movie: Movie {title: '${title}', releaseDate: date('${releaseDate}'), genre: '${genre}'}) RETURN movie`));
        session.close();

        res.status(200).send({"movieAdded": response.records[0]._fields[0]});
    } catch(ex) {
        res.send(ex)
    }
});

router.put('/', async (req, res) => {
    const { title, releaseDate, genre } = req.body;
    const session = driver.session();
    try {
        const response = await session.writeTransaction((tx) => 
            tx.run(`MATCH (movie: Movie) WHERE ID(movie) = ${req.params.id} SET movie.title: '${title}', movie.releaseDate: date('${releaseDate}'), movie.genre: '${genre}' RETURN *`));
        session.close()
        console.log(response);
        if (response.records === []){
            return res.status(400).send({})
        } else {
            return res.status(200).send({"movieUpdated": response.records[0]._fields[0]})
        }
    } catch(ex) {
        return res.status(500).send(ex);
    }
});

router.delete('/:id', async (req, res) => {
    const session = driver.session();
    try {
        const response = await session.writeTransaction((tx) => 
            tx.run(`MATCH (movie: Movie) WHERE ID(movie) = ${req.params.id} DELETE movie RETURN *`));
        session.close()
        console.log(response);
        if (response.records === []){
            return res.status(400).send({})
        } else {
            return res.status(200).send({"movieRemoved": response.records[0]._fields[0]})
        }
    } catch(ex) {
        return res.status(500).send(ex);
    }
});

router.post('/assign-actor', async (req, res) => {
    const { actorId, movieId } = req.body; 
    const session = driver.session();
    try{
        const response = await session.writeTransaction(tx => 
            tx.run(`MATCH (a: Actor) , (b: Movie) WHERE ID(a) = ${actorId} AND ID(b) = ${movieId} MERGE (a)-[r:ACTED_ID]->(b) RETURN r`))
        session.close();
        console.log(response);
    } catch(ex){
        return res.status(500).send(ex)
    }
    return res.send({});
});

module.exports = router;
